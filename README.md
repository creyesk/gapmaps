# Gapmaps (Full Version)

Este paquete contiene las herramientas esenciales para hacer lo siguiente:
    - Descargar artículos de science direct a través del api elsapy (a este se le han hecho unas modificaciones para poder extraer los textos en formato estructurado xml.)
    - Almacenar los archivos en una base de datos postgres
    - Extraer información relevante de los artículos como keywords, secciones y los textos de cada sección.
    - A partir de  
## Instalar la base de datos
1. Instalar el cliente de postgres 

    ```
    sudo apt update
    sudo apt install postgresql postgresql-contrib
    ```

2. Crear la base de datos gapmaps_deploy y el usuario que se conectará a la base

    ~~~
    sudo -i -u postgres
    createuser --interactive
    createdb gapmaps
    exit
    ~~~

3. Crear contraseña para el usuario y dar permisos a la base de datos

    ~~~
    sudo -u postgres psql
    ALTER USER <user> WITH ENCRYPTED PASSWORD '<password>';
    GRANT ALL PRIVILEGES ON DATABASE gapmaps_deploy to <user>;
    ~~~

## Descargar los artículos
1. En el archivo *queries.py* consiste de un diccionario grande. Cada llave corresponde a un Gapmap, donde se especifican las búsquedas que se hacen en scidirect para descargar los articulos correspondientes. En el archivo *queries.py* se explica como llenar los campos de este diccionario. 
2. Una vez se hayan especificado los queries se corre el archivo *download.py*. Esto se debe hacer desde un computador con acceso a las bases de datos de science direct, de lo contrario solo se descargaran los abstracts de los artículos.
~~~
python download.py
~~~
Si se desea descargar únicamente los artículos para cierto o ciertos gapmaps se debe especificar con la opción --subset o -s. e.g.
~~~
python download.py -s crime drugs
~~~
Se llevara un registro de los artículos en download_list.json con el fin de poder reanudar la descarga en caso de ser interrunpida sin volver a descargar los archivos obtenidos.

## Explicación modelos de Django
La aplicación está organizada en varias objetos. Sus campos y sus relaciones se ilustran en la siguiente figura:
![map_models](my_project.png)

Este mapa se puede generar con el comando
~~~
python manage.py graph_models -a -g -I TopicModel,Gapmaps,Corpus,Vocabulary,Query,SectionList,Section,Article, -o my_project.png
~~~

## Subir los artículos a la base de datos y crear los gapmaps:
Esta parte requiere de multiples pasos que se encuentran resumidos en el archivo *build_from_scratch.py*. Los pasos que ejecuta este script se describen a continuación:
1. load_articles - Toma los artículos grabados por elsappy.FullDoc.write() y crea un objeto Article en Django el cual se almacena en la base de datos.
2. exc = SectionList(id='Exclude') Crea una lista de secciones. Luego de el paso 6 (update_sections) se debe elegir las secciones que se desean excluir del Corpus (Secciones como *Overview* usalmente contienen descripciones de metodologías que se usan en otros artículos pero no en el actual y por lo tanto deberían excluirse esas secciones del Corpus para minizar el ruido.)
3. load_queries_and_corpora - Crea los objetos corpora para cada gapmap, y a cada Corpus lo vincula con los queries pertenecientes a ese gapmap.
4. update_queries() - Carga las queries de archivos json creados por elsapy con los resultados de las búsquedas. En el caso de pasar el argumento overwrite=True, repite la consulta a scidir y guarda los resultados de los artículos en una lista. 
5. update_articles() - Itera sobre cada una de los queries y se cerciora que todos los artículos correspondientes a ese query se encuentre en la base de datos y vinculado al query.
6. update_sections() - Itera sobre el texto de los artículos buscando los nombres de las secciones comunes (que aparezcan en más de min_df=100 artículos). Cuando se complete este punto se debe acceder al objecto SectionList(id='Exclude') para elegir los titulos de las secciones que se desean excluir. 
7. load_corpora() - Para cada Corpus toman todos los artículos que le corresponden e itera sobre sus textos. Para cada artículo extrae su texto completo excluyendo las secciones especificadas en el paso anterior. Cada texto se guarda como un string en una lista que será el insumo para los modelos de tópicos (Document matrix). Esto puede tomar un buen tiempo y acuparar bastante memoria dependiendo de la cantidad de artículos para cada Corpus. 
8. load_vocabs() - Se carga el vocabulario de metodologías, el cual viene dado en el archivo *methodologies.py*. Se carga el vocabulario de subjects para cada gapamap el cual viene dado por todas las keywords de los artículos de ese cada Corpus (con la opción por default de excluir las metodologias). 
9. load_topics() - Se cargan dos tópicos para cada gapmap: subjects y methodologies. Sus características están especificadas en *topics.py*. 
10. load_gapmaps() - Con todo la información se genera cada uno de los gapmaps. Esto puede ser demorado y dependiendo de el número de artículos en cada Corpus puede necesitar bastante memoria. 