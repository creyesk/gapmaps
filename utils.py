from api.models import Article
import json, os, pdb

var_dict = {
    'id': 'pii',
    'title': 'dc:title',
    'date': 'prism:coverDate',
    'abstract': 'dc:description',
    'author': 'dc:creator',
    'keywords': 'dcterms:subject'
}

def _extract(d, what, where, l=[]):
    for k, v in d.items():
        if isinstance(v, list):
            v = dict(zip(range(len(v)), v))
        if (k in where) & (k in what):
            l.append({k: v})
        elif k in where:
            _extract(v, what, what, l)
        elif isinstance(v, dict):
            _extract(v, what, where, l)
    return l

def _get_text(d, types = ['#text', 'ce:bold',
                          'ce:italic', 'ce:sans-serif']):
    ttype = [k for k in types if k in d.keys()]
    if ttype:
        if isinstance(ttype[0],str):
            return d[ttype[0]]
    else:
        return None

def _parse_sections(l):
    d = {}
    repeated = {}  # handles duplicate sections
    for item in l:
        if 'ce:section-title' in item.keys():
            if isinstance(item['ce:section-title'], dict):
                title = _get_text(item['ce:section-title'])                
            elif isinstance(item['ce:section-title'], str):
                title = item['ce:section-title']
            else:
                break
            if isinstance(title, str):
                title = title.lower()
                repeated[title] = 0
                if title in d.keys():
                    repeated[title] += 1
                    title = '{}_{}'.format(title,
                                           str(repeated[title]).zfill(2))
                d[title] = ''
            else:
                print(title)
                break
        elif '#text' in item.keys():
            if 'title' in locals():
                d[title] = d[title] + '\n' + item['#text']
    return d

def _parse_keywords(l):
    keywords = set()
    for d in l:
        if isinstance(d['ce:text'], dict):
            keyword = _get_text(d['ce:text'])
        elif isinstance(d['ce:text'], str):
            keyword = d['ce:text']
        if keyword:
            keywords.update([keyword.lower()])
    return list(keywords)

def add_article(art_dict):
    """ 
    Takes an article in dictionary type as saved by the elsapy. Parses the
    article's sections and metadata and saves it to the database.the
    """

    core = art_dict['coredata']
    text = art_dict['originalText']
    pii = core['pii']
    try:
        art = Article.objects.get(id=pii) 
    except:
        art = Article(id=pii,
                      title=core[var_dict['title']],
                      date=core[var_dict['date']],
                      full_text=text)
    keywords = core.get(var_dict['keywords'], [])
    if isinstance(keywords, str) and len(keywords) < 256:
        art.keywords = [keywords]
    else:
        # try:
        #     art.keywords = [x['$'] for x in keywords if len(x['$']) < 256]
        # except TypeError:
        art.keywords = [x for x in keywords if len(x) < 256]
    
    for x in art.keywords:
        assert isinstance(x, str)
    art.abstract = core.get(var_dict['abstract'], '')
    if art.abstract is None:
        art.abstract = ''
    
    author = core.get(var_dict['author'], '')
    if isinstance(author, list):
        art.author = author[0]
    elif isinstance(author, str):
        art.author = author
    
    sections = _extract(text, what=['ce:section-title','#text'],
                              where=['ce:sections'], l=[])
    art.section_text = _parse_sections(sections)
    if art.keywords:
        art.section_text['keywords'] = ' '.join(art.keywords)
    art.save()

def query_results(query, query_dir):
    """
    Gets the queries results from the file written by elsapy
    """
    f_title = query[0:min(100,len(query))] + '.json'
    f_path = os.path.join(query_dir, f_title)
    try:
        with open(f_path, 'r') as f:
            q_dict = json.load(f)
        results = q_dict[query] 
        if isinstance(results, list):
            return results
        return []
    except FileNotFoundError:
        print('Query: {} not in directory.'.format(query))
        return None


def articles_in_db():
    """
    Returns a list of articles stored in the database
    """
    articles = Article.objects.only('id').all().iterator()
    db_list = []
    for art in articles:
        db_list.append(art.id)
    return db_list
