"""
Especifies parameters needed for the construction of the LDA topic models
including the corpus and vocabulary.
"""



subject_vec_params = {'min_df': 3,
					  'max_df': 0.95,
					  'ngram_range': (1,3)}
method_vec_params = {'min_df': 3,
					 'max_df': 0.95,
					 'ngram_range': (2,3)}

topic_models = {
	'drug_subjects' : {
		'vocabulary':'drugs_subjects',
	    'corpus':'drugs_corpus',
	    'queries':'drugs',
	    'vectorizer_params':subject_vec_params,
	    'n_topics': 20
	},
	'drug_methodologies' : {
		'vocabulary':'Methodologies',
		'corpus':'drugs_corpus',
		'queries':'drugs',
		'vectorizer_params':method_vec_params,
		'n_topics': 10
	},
	'crime_subjects' : {
		'vocabulary':'crime_subjects',
	    'corpus':'crime_corpus',
	    'queries':'crime',
	    'vectorizer_params':subject_vec_params,
	    'n_topics': 20
	},
	'crime_methodologies' : {
		'vocabulary':'Methodologies',
		'corpus':'crime_corpus',
		'queries':'crime',
		'vectorizer_params':method_vec_params,
		'n_topics': 10
	}
}