"""
Both topic models (subjects and methodologies) are specified
for each gapmap.
"""

gapmaps = {
	'Drugs': {
		'subjects': 'drug_subjects',
		'methodologies': 'drug_methodologies'
	},
	'Crime': {
		'subjects': 'crime_subjects',
		'methodologies': 'crime_methodologies'
	},
}