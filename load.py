from methodologies import methodologies
from maps import gapmaps
from queries import queries_dict, build_queries
from api.models import Article, Section, Query, Vocabulary, TopicModel
from api.models import Corpus, Gapmap
from itertools import product, chain
from utils import _extract, _parse_sections, add_article, query_results
from topics import topic_models


import pdb
import os
import json

data_dir = '/home/carlos/Documents/Gapmaps/gapmaps/data'

def load_articles(dir_path, delete=False):
    """
    Takes the articles given in files by the elsapy.FullDoc write
    function and adds them as Article.
    """
    if delete:
        for art in Article.objects.all().iterator():
            art.delete()
    for folder in os.listdir(dir_path):
        art_path = os.path.join(dir_path, folder, 'articles')
        for i, file in enumerate(os.listdir(art_path)):
            with open(os.path.join(art_path, file)) as f:
                art_dict = json.load(f)
                pii = art_dict['coredata']['pii']
            print('Adding article {}:{}'.format(i, pii))
            add_article(art_dict)

def load_queries_and_corpora(data_dir=None, delete=False):
    """
    Takes the queries specified for each gapmap in queries.py and
    creates the Corpus object that corresponds to that gapmap
    (No text is added to Corpus by this function). Adds each query
    in queries_dict[gapmap] as a Query object and links it to the 
    Corpus for the same gapmap.
    """
    if delete:
        print('Deleting queries and corpora')
        Query.objects.all().delete()
        Corpus.objects.all().delete()
    for k, v in queries_dict.items():
        c_name = k + '_corpus'
        print('Loading {} queries and corpus'.format(k))
        try:
            c = Corpus.objects.get(name=c_name)
        except:
            c = Corpus(name=c_name)
            c.save()
        queries = build_queries(subset=[k])     
        for query in queries:
            try:
                q = Query.objects.get(query=query[1]) 
            except:
                q = Query(query=query[1])
            q.g_map = k
            if data_dir:
                query_dir = os.path.join(data_dir, k, 'queries')
                q.results = query_results(query[1], query_dir)
            q.save()
            c.queries.add(q)
        c.save()

def load_vocabs(include_methodologies=False):
    """
    Creates a Vocabulary object for the methodologies from the list given in 
    methodologies.py.

    Creates a subject vocabulary for each gapmap which is built from all the
    keywords contained in the articles belonging to that gapmap (The 
    relationship is specified by the Corpus of the gapmap).

    If include_methodologies==False, words from the methodologies vocabulary
    excluded from the subjects vocabulary. 
    """
    print('Loading methodologies')
    vocab_method = Vocabulary(name='Methodologies')
    vocab_method.words = list(set(methodologies))
    vocab_method.save()

    print('Loading subjects...')

    if include_methodologies == False:
        methods = Vocabulary.objects.get(name='Methodologies').words
    
    for g, d in queries_dict.items():
        v = Vocabulary(name=g + '_subjects')
        v.stopwords = d['exclude']
        cross_terms = [ct + ' ' +  cw for ct, cw 
                       in product(d['cross_terms'], d['cross_with'])]
        v.query_words = d['terms'] + cross_terms + d['one_word_terms']
        c = Corpus.objects.get(name=g + '_corpus')
        
        # get all subjects from keywords in related queries
        s = set()
        for q in c.queries.all():
            articles = q.articles.all().iterator()
            for i, art in enumerate(articles):
                print('Processing article', i, end="\r")
                if art.keywords:
                    w = [x.lower() for x in art.keywords if len(x.split(' '))>1]
                    if include_methodologies:
                        s.update(w)
                    else:
                        kwds = set(w).difference(methods)
                        s.update(kwds)
        s.update(v.query_words)
        v.words = list(s)

        m = '{} Vocabulary has {} words'
        print(m.format(g, len(v.words)))
        v.save()

def load_topics():
    """
    Creates the TopicModel object for every object specified in topics.py.
    Links the topic model to its corpus and vocabulary and gives the parameters
    for the LDA_model. 
    """
    for n, topic in topic_models.items():
        if TopicModel.objects.filter(name=n).exists():
            TopicModel.objects.get(name=n).delete()
        print('Loading topic {}...'.format(n))
        v = Vocabulary.objects.get(name=topic['vocabulary'])
        v.stopwords = queries_dict[topic['queries']]['exclude']
        v.save()
        c = Corpus.objects.get(name=topic['corpus'])
        t = TopicModel(corpus=c, vocabulary=v, name=n)
        t.vectorizer_params = topic['vectorizer_params']
        t.n_topics = topic['n_topics']
        t.save()

def load_gapmaps():
    """
    Creates a gapmap object for every element specified in maps.gapmaps.
    Every gapmaps needs a two topic models: one for methodologies and
    the other one for subjects. Both topic models are specified in 
    maps.gapmaps and are constructed in this function (g.generate_gapmap
    calls api.models.TopicModels on both its topic models.) 
    """
    for n, gapmap in gapmaps.items():
        if Gapmap.objects.filter(name=n).exists():
            Gapmap.objects.get(name=n).delete()
        print('Loading gapmap {}...'.format(n))
        m = TopicModel.objects.get(name=gapmap['methodologies'])
        s = TopicModel.objects.get(name=gapmap['subjects'])
        g = Gapmap(subjects=s, methodologies=m, name=n)
        g.generate_gapmap()
        g.save()