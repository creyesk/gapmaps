var auth_token;

function get_gapmaps_list(){
    $.getJSON("../gapmaps/list", function(json_data){
        data = json_data;
        $("#lista_gapmaps").empty();
        for (i=0;i<data.results.length;i++){
          var id_gapmap = data.results[i].id;
          var name_gapmap = data.results[i].name;
          var $li = $("<li onclick=" + "'_myFunction(" + id_gapmap + ")'" + ">" + name_gapmap + "</li>");
          $("#lista_gapmaps").append($li);
      }
  });
};

function download_topics_meths(id){
    $.getJSON("../gapmaps/".concat(id).concat("/topicsdata"), function(json_data){
        data = json_data;
        draw_topics_meths(data);
        var helpi = d3.select("#help_lda")
        .append("svg")
        .attr("width", 480)
        .attr("height", 240);

        helpi.append("rect")
        .attr("x", 0)
        .attr("y", 20)
        .attr("height", 15)
        .attr("width", 100)
        .style("fill", "rgb(108,113,196)")
        .attr("opacity", 0.4);

        helpi.append("text")
        .attr("x", 105)
        .attr("y", 30)
        .style("dominant-baseline", "middle")
        .style("fill", "rgba(255,255,255,0.7)")
        .text("Overall term frequency");

        helpi.append("rect")
        .attr("x", 0)
        .attr("y", 40)
        .attr("height", 15)
        .attr("width", 50)
        .style("fill", "rgba(181,137,0,1)")
        .attr("opacity", 0.8);

        helpi.append("text")
        .attr("x",55)
        .attr("y", 50)
        .style("dominant-baseline", "middle")
        .style("fill", "rgba(255,255,255,0.7)")
        .text("Estimated term frequency within the selected topic");

        helpi.append("a")
        .attr("xlink:href", "http://vis.stanford.edu/files/2012-Termite-AVI.pdf")
        .attr("target", "_blank")
        .append("text")
        .attr("x", 0)
        .attr("y", 70)
        .style("dominant-baseline", "middle")
        .style("fill", "rgba(255,255,255,0.7)")
        .text("1. saliency(term w) = frequency(w) * [sum_t p(t | w) * log(p(t | w)/p(t))] for topics t; see Chuang et. al (2012)");

        helpi.append("a")
        .attr("xlink:href", "http://nlp.stanford.edu/events/illvi2014/papers/sievert-illvi2014.pdf")
        .attr("target", "_blank")
        .append("text")
        .attr("x", 0)
        .attr("y", 85)
        .style("dominant-baseline", "middle")
        .style("fill", "rgba(255,255,255,0.7)")
        .text("2. relevance(term w | topic t) = \u03BB * p(w | t) + (1 - \u03BB) * p(w | t)/p(w); see Sievert & Shirley (2014)");
    });
};

function download_gapmaps(id) {
    $.getJSON("../gapmaps/".concat(id), function(json_data){
       data = json_data;
       draw_matrix_d3(data);
       draw_articles_volume(data);
});
};

function draw_matrix_d3(data) {
    var titulo_gapmap = data.name;
    $("#titulo_gapmap").html(titulo_gapmap);

    //id's para controlar gráficos LDA
    // //var id_topicos = document.getElementById("descripcion_topicos").childNodes[1].id;
    // var id_metodologias = document.getElementById("descripcion_metodologias").childNodes[1].id;
    // var id_topicos = data.subjects.tm_id;
    var id_metodologias = data.methodologies.tm_id;
    var id_topicos = data.subjects.tm_id;
    //matriz que ingresa
    var heatmap = data.heatmap;

    //número de filas de la matriz que ingresa
    var numero_metodologias = heatmap.length;

    //número de columnas de la matriz que ingresa
    var numero_topicos = heatmap[0].length;

    //dimensiones de la matriz que ingresa
    var dimensiones = numero_metodologias * numero_topicos;

    //maximo y minimo de toda la matriz (pesos)
    var minimos = [];
    var maximos = [];

    for (i=0;i<numero_metodologias;i++){
        var max = heatmap[i].reduce(function(a, b) {
            return Math.max(a, b);
        });
        var min = heatmap[i].reduce(function(a, b) {
            return Math.min(a, b);
        });
        maximos.push(max);
        minimos.push(min);
    }

    var maximo = maximos.reduce(function(a, b) {
        return Math.round(Math.max(a, b));
    });

    var minimo =  minimos.reduce(function(a, b) {
        return Math.round(Math.min(a, b));
    });
    //nombres y numeros de los tópicos
    var nombre_topicos = data.subjects.topic_names;
    var nombre_topicos_numero = [];
    var topicos_numero = [];
    var counter_nombre_topicos = 0;
    for (i=0;i<=nombre_topicos.length-1;i++){
        counter_nombre_topicos++;
        nombre_topicos_numero.push(counter_nombre_topicos + ". " + nombre_topicos[i]);
        topicos_numero.push(counter_nombre_topicos);
    }

    //nombres y numeros de las metodologías
    var nombre_metodologias = data.methodologies.topic_names;
    var nombre_metodologias_numero = [];
    var metodologias_numero = [];
    var counter_nombre_metodologias = 0;
    for (i=0;i<=nombre_metodologias.length-1;i++){
        counter_nombre_metodologias++;
        nombre_metodologias_numero.push(counter_nombre_metodologias + ". " + nombre_metodologias[i]);
        metodologias_numero.push(counter_nombre_metodologias);
    }

    //ids para enlaces top articulos de cada combinación
    var ids_top_articulos = data.top_article_ids;

    //nombres de los top articulos de cada combinación
    var nombres_top_articulos = data.top_article_ids_readable;

    //numeros para notificacion tópico y meto.
    var numero_metodologias_noti = document.getElementById('notification_metho');
    var numero_topicos_noti = document.getElementById('notification_topic');

    //elementos DOM para titulos y lista_articulos
    var titulo_metodologias = document.getElementById('titulo_met');
    var titulo_topicos = document.getElementById('titulo_top');
    var numero_metodologias_pos = document.getElementById('numero_met');
    var numero_topicos_pos = document.getElementById('numero_top');
    var lista_articulos = document.getElementById('lista_articulos');

    //datos para la escala de color
    var rectangulos_array = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1];
    var minimo_maximo = [0,40];

    //tooltip para matriz
    var tooltip = d3.select("body")
        .append("div")
        .attr("class", "tooltip_2")
        .style("position", "absolute")
        .style("z-index", "1000000")
        .style("visibility", "hidden")
        .text("error");

    //dibujo en D3 V.3.3
    var margin = {top: 100, right: 100, bottom: 100, left: 120},
        width = 610,
        height = 200;

    var escala_x = d3.scale.ordinal()
        .domain(d3.range(numero_topicos))
        .rangeBands([0, width]);

    var escala_y = d3.scale.ordinal()
        .domain(d3.range(numero_metodologias))
        .rangeBands([0, height]);

    var escala_color = d3.scale.linear()
        .domain([minimo,maximo])
        .range([0,1]);

    var svg = d3.select("#matriz").append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    /*svg.append("rect")
    .attr("class", "background")
    .attr("width", width)
    .attr("height", height);*/

    var id_counter = 0;

    var fila = svg.selectAll(".fila")
        .data(heatmap)
        .enter()
        .append("g")
        .attr("class", "fila")
        .attr("transform", function(d, i) { return "translate(0," + escala_y(i) + ")"; });

    fila.selectAll(".celda")
        .data(function(d) { return d; })
        .enter()
        .append("rect")
        .attr("class","celda")
        .attr("x", function(d, i) { return escala_x(i); })
        .attr("width", escala_x.rangeBand())
        .attr("height", escala_y.rangeBand())
        .attr("id",function(d,i) {
            id_counter++;
            return id_counter;
        })

    .style("fill",function(d) {
      var opacity = escala_color(d);
      return "rgba(38,139,210," + opacity + ")";
    })
    .on('click', function(d,i) {
        //guardo el id del elemento clickeado
        var id = d3.select(this).attr("id");

        //esta funcion me retorna un array con el equivalente del id a la posicion en (x,y)
        var array_posicion = _calcular_posicion(id,numero_metodologias,numero_topicos,dimensiones);
        var posicion_metodologia = array_posicion[0];
        var posicion_topico = array_posicion[1];

        //a partir de la posicion X y Y del elemento en la matriz se llama la información asociada a este.
        var topicoSeleccionado = nombre_topicos_numero[array_posicion[1]-1];
        var metodologiaSeleccionada = nombre_metodologias_numero[array_posicion[0]-1];

        //actualizo borde según el elemento seleccionado.
        d3.selectAll('.celda').style("stroke", "rgba(0,0,0,0)");
        d3.selectAll('.celda').style("stroke-width", "4");
        d3.select(this).style("stroke", "orange");
        d3.select(this).style("stroke-width", "4");

        $(lista_articulos).empty();
        $(titulo_metodologias).html(metodologiaSeleccionada + ",");
        $(titulo_topicos).html(topicoSeleccionado);
        $(numero_metodologias_noti).html(posicion_metodologia);
        $(numero_topicos_noti).html(posicion_topico);

        //$(numero_metodologias_pos).html("Metodología (fila): " + posicion_metodologia);
        //$(numero_topicos_pos).html("Tópico (columna): " + posicion_topico);

        var array_top_articulos_ids = ids_top_articulos[array_posicion[0]-1][array_posicion[1]-1];
        var array_top_articulos_nombres = nombres_top_articulos[array_posicion[0]-1][array_posicion[1]-1];
        var link_sevier = "http://www.sciencedirect.com/science/article/pii/";

        if (heatmap[array_posicion[0]-1][array_posicion[1]-1]>0){   
            for (i=0;i<array_top_articulos_ids.length;i++){
                var $li = $("<li>" + "<a href='" + link_sevier + array_top_articulos_ids[i] + "' target='" + "_blank" + "'>" + array_top_articulos_nombres[i] + " " + "<span class='" + "glyphicon glyphicon-globe" + "'>" + "</a>" +  "</li>");
                $(lista_articulos).append($li);
            }
        }
      //dispatchEvent para actualizar gráficos LDA
        var click_metodologia = new MouseEvent("click");

        var mouse_out_metodologia = new MouseEvent("mouseup");


        var uno = document.getElementById(id_metodologias + "-topic" + posicion_metodologia).dispatchEvent(click_metodologia);
        var dos = document.getElementById(id_metodologias + "-topic" + posicion_metodologia).dispatchEvent(mouse_out_metodologia);

        var tres = document.getElementById(id_topicos + "-topic" + posicion_topico).dispatchEvent(click_metodologia);
        var cuatro = document.getElementById(id_topicos + "-topic" + posicion_topico).dispatchEvent(mouse_out_metodologia);
    })
    .on('mouseover', function(d,i) {
         var array_posicion = _calcular_posicion(this.id,numero_metodologias,numero_topicos,dimensiones);
         var topicoSeleccionado = nombre_topicos_numero[array_posicion[1]-1];
         var metodologiaSeleccionada = nombre_metodologias_numero[array_posicion[0]-1];
         tooltip.style("visibility", "visible")
         tooltip.html(metodologiaSeleccionada + " , " + topicoSeleccionado + "<br>" + "Weighted: " + Math.round(d) );
    })

    .on("mousemove", function(){return tooltip.style("top",
      (d3.event.pageY-10)+"px").style("left",(d3.event.pageX+10)+"px");})

    .on("mouseout", function(){return tooltip.style("visibility", "hidden");});

    var fila_etiqueta_nombre = svg.selectAll(".fila_etiqueta")
        .data(nombre_metodologias)
        .enter().append("g")
        .attr("class", "row_labels")
        .attr("transform", function(d, i) { return "translate(0," + escala_y(i) + ")"; });

    fila_etiqueta_nombre.append("line")
        .attr("x1", -width);

    fila_etiqueta_nombre.append("text")
        .attr("x", "-16")
        .attr("y", escala_y.rangeBand() / 2)
        .attr("dy", ".32em")
        .attr("text-anchor", "end")
        .style("fill","white")
        .text(function(d) { return d; });

    var fila_etiqueta_numero = svg.selectAll(".fila_etiqueta")
        .data(metodologias_numero)
        .enter().append("g")
        .attr("class", "row_labels")
        .attr("transform", function(d, i) { return "translate(0," + escala_y(i) + ")"; });

    fila_etiqueta_numero.append("line")
    .attr("x1", -width);

    fila_etiqueta_numero.append("text")
        .attr("x", "-2")
        .attr("y", escala_y.rangeBand() / 2)
        .attr("dy", ".32em")
        .attr("text-anchor", "end")
        .style("fill","rgb(181,137,0)")
        .text(function(d) { return d; });

    var columna_etiqueta_nombre = svg.selectAll(".columna_etiqueta")
        .data(nombre_topicos)
        .enter().append("g")
        .attr("class", "column_labels")
        .attr("transform", function(d, i) { return "translate(" + escala_x(i) + ")rotate(-45)"; });

    columna_etiqueta_nombre.append("line")
    .attr("x1", -width);

    columna_etiqueta_nombre.append("text")
        .attr("x", 22)
        .attr("y", (escala_y.rangeBand() / 4)-10)
        .attr("dy", ".32em")
        .attr("text-anchor", "start")
        .style("fill","white")
        .text(function(d, i) { return d; });

    var columna_etiqueta_numero = svg.selectAll(".columna_etiqueta")
        .data(topicos_numero)
        .enter().append("g")
        .attr("class", "column_labels")
        .attr("transform", function(d, i) { return "translate(" + escala_x(i) + ")rotate(0)"; });

    columna_etiqueta_numero.append("line")
        .attr("x1", -width);

    columna_etiqueta_numero.append("text")
        .attr("x", 12)
        .attr("y", (escala_y.rangeBand() / 4)-12)
        .attr("dy", ".32em")
        .attr("text-anchor", "start")
        .style("fill","rgb(181,137,0)")
        .text(function(d, i) { return d; });
    
    var margin = {top: 10, right: 5, bottom: 10, left: 5},
    width = 60 - margin.left - margin.right,
    height = 195 - margin.top - margin.bottom;

    var escala = d3.scale.linear()
    .domain([minimo,maximo])
    .range([height,0]);

    var eje = d3.svg.axis().scale(escala).orient("right");

    var lienzo = d3.select("#matriz")
        .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .attr("class", "svg_escala")
        .append("g")
        .attr("transform","translate(" + margin.left + "," + margin.top + ")");

    lienzo.append("g")
        .selectAll("rect")
        .data(rectangulos_array)
        .enter()
        .append("rect")
        .attr("class", "cuadros")
        .attr("width", "25px")
        .attr("height", "15px")
        .attr("y",function(d) { return (((d * 10) * 16) * -1) + (height-15); })
        .style("opacity", function(d) { return d; });

    lienzo.append("g")
        .attr("class", "ejeBlanco")
        .attr("transform","translate(20,2)")
        .call(eje);
}

function draw_articles_volume(data) {

    var articulosLayout = {
        paper_bgcolor: 'transparent',
        plot_bgcolor: 'transparent',
        height: 140,
        width: 200,
        yaxis: {
            titlefont: {
            color: "fff",
            size: 10
            },
            range: [
                -14.777777777777791,
                300.777777777778
            ],
            type: "linear",
            autorange: true,
            title: "# of Articles",
            tickfont: {
            size: 10,
            color: '#839496'
            },
            gridcolor: 'rgba(0,0,0,0.0)'
        },
        margin: {
            r: 0,
            b: 40,
            l: 40,
            t: 0
        },
        breakpoints: [],
        xaxis: {
            titlefont: {
                color: "fff",
                size: 10
            },
            range: [
                2000,
                2016
            ],
            type: "linear",
            autorange: true,
            title: "Date",
            tickfont: {
                size: 10,
                color: '#839496'
            },
            gridcolor: 'rgba(0,0,0,0.0)'
        }
    };
    var volumenArticulosData = [{
        x: data.years,
        y: data.volume,
        type: 'scatter',
        marker:{
            color:"rgba(211,54,130,1)"
        }
    }];
    var volumen_articulos = document.getElementById('volumen_articulos');
    Plotly.purge(volumen_articulos);
    Plotly.plot( volumen_articulos, volumenArticulosData, articulosLayout,{ displayModeBar: false});
};

function draw_topics_meths_time(data) {

    var distributionLayout = {
      paper_bgcolor: 'transparent',
      plot_bgcolor: 'transparent',
      barmode: "stack",
      font:{
        color:'rgba(255,255,255,0.8)'
    },
    yaxis: {
        titlefont: {
          color: "fff",
          size: 18
      },
      range: [
      0,
      1.0526315789473688
      ],
      tickfont: {
          size: 12,
          color: 'white'
      },
      type: "linear",
      autorange: true,
      title: "# de Artículos"
  },
  breakpoints: [],
  xaxis: {
    titlefont: {
      color: "fff",
      size: 18
  },
  range: [
  2000.5,
  2016.5
  ],
  tickfont: {
      size: 12,
      color: 'white'
  },
  type: "linear",
  autorange: true,
  title: "Fecha"
    }
    };

    var topicosTiempoData = [];
    var j = 0;
    for (i=0;i<data.subjects_distribution.length;i++){
        j++;
        topicosTiempoData.push({
            y: data.subjects_distribution[i],
            x: data.years,
            type: 'bar',
            name: 'Tópico ' + j,
        })
    };

    var metodologiasTiempoData = [];
    var g = 0;

    for (i=0;i<data.methodologies_distribution.length;i++){
        g++;
        metodologiasTiempoData.push({
            y: data.methodologies_distribution[i],
            x: data.years,
            type: 'bar',
            name: 'Metodología ' + g,
        })
    };
    var topicosTiempo = document.getElementById('topicos_tiempo');
    var metodologiasTiempo = document.getElementById('metodologias_tiempo');

    Plotly.purge(topicosTiempo);
    Plotly.plot( topicosTiempo, topicosTiempoData,distributionLayout,{ displayModeBar: false});
    Plotly.purge(metodologiasTiempo);
    Plotly.plot( metodologiasTiempo, metodologiasTiempoData, distributionLayout,{ displayModeBar: false});
};

function draw_topics_meths(data){
    $("#descripcion_topicos").html(data.subj);
    $("#descripcion_metodologias").html(data.method);
};

function _calcular_posicion(id,num_filas_met,num_columnas_top) {
    var posicion = [];
    var pos_fila = Math.ceil((id / num_columnas_top));
    var pos_columna = Math.ceil((id % num_columnas_top));
    if (pos_columna === 0){
      pos_columna = num_columnas_top;
  }
  posicion.push(pos_fila,pos_columna);
  return posicion;
}

function _calcular_id(num_filas_met,num_columnas_top,pos_x,pos_y) {
    var id = ((num_columnas_top * pos_x)-(num_columnas_top-1)) + (pos_y-1);
    return id;
}

function _myFunction(id){
    get_gapmaps_list();
    // document.getElementById("matriz").innerHTML = "";
    //document.getElementById("lista_articulos").innerHTML = "";
    $("#matriz").empty();
    $("#lista_articulos").empty();
    //$("#numero_met").html("Metodología (fila): 0");
    //$("#numero_top").html("Tópico (columna): 0");
    $("#notification_metho").html("0");
    $("#notification_topic").html("0");
    $("#titulo_met").html("Nothing Selectes");
    $("#titulo_top").html("");

    $("#help_lda").empty(); 

    $("#descripcion_metodologias").remove();
    $("#descripcion_topicos").remove();
    $("#descripcion_metodologias_tab").append( "<div id=" + '"descripcion_metodologias"' + "class=" + '"graficas_lda"' + "></div>" );
    $("#descripcion_topicos_tab").append( "<div id=" + '"descripcion_topicos"' + "class=" + '"graficas_lda"' + "></div>" );

    download_topics_meths(id);
    download_gapmaps(id);
}

$(document).ready(function() {
    var id = 5;
    get_gapmaps_list();
    download_topics_meths(id);
    download_gapmaps(id);
})
