from django.contrib import admin

from . models import Article, Query, SectionList, Corpus
from . models import Vocabulary, TopicModel, Gapmap

admin.site.register(Article)
admin.site.register(SectionList)
admin.site.register(Query)
admin.site.register(Corpus)
admin.site.register(Vocabulary)
admin.site.register(TopicModel)
admin.site.register(Gapmap)
