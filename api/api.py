from rest_framework import generics, permissions
import random, math

from .serializers import GapmapSerializer, ArticleSerializer
from . serializers import TopicModelSerializer, SimpleGapmapSerializer
from .models import Gapmap, Article, TopicModel

class GapmapList(generics.ListCreateAPIView):
    model = Gapmap
    queryset = Gapmap.objects.all()
    serializer_class = GapmapSerializer
    permission_classes = [
#        permissions.IsAuthenticated,
    ]

class SimpleGapmapList(generics.ListCreateAPIView):
    model = Gapmap
    queryset = Gapmap.objects.all()
    serializer_class = SimpleGapmapSerializer
    permission_classes = [
#        permissions.IsAuthenticated,
    ]

class GapmapDetail(generics.RetrieveUpdateDestroyAPIView):
    model = Gapmap
    queryset = Gapmap.objects.all()
    serializer_class = GapmapSerializer
    permission_classes = [
#        permissions.IsAdminUser,
    ]

    
class ArticleList(generics.ListCreateAPIView):
    model = Article
    queryset = Article.objects.all()
    serializer_class = ArticleSerializer
    permission_classes = [
#        permissions.IsAuthenticated,
    ]

class ArticleDetail(generics.RetrieveUpdateDestroyAPIView):
    model = Article
    queryset = Article.objects.all()
    serializer_class = ArticleSerializer
    permission_classes = [
#        permissions.IsAdminUser,
    ]
