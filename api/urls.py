from django.urls import path, include

from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework.authtoken.views import obtain_auth_token
from . api import GapmapList, GapmapDetail, ArticleList
from . api import ArticleDetail, SimpleGapmapList

from . views import base, main, topicsdata
from rest_framework_swagger.views import get_swagger_view

site_urls = [
	path(r'', main, name='main'),
]

gapmap_urls =  [
    path(r'<int:pk>', GapmapDetail.as_view(), name='gapmap-detail'),
    path(r'', GapmapList.as_view(), name='gapmap-list'),
    path(r'list', SimpleGapmapList.as_view(), name='simple-gapmap-list'),
    path(r'<int:pk>/topicsdata', topicsdata, name='topicsdata'),
]

article_urls = [
    path(r'<int:article-detail>', ArticleDetail.as_view(), name='article-detail'),
    path(r'', ArticleList.as_view(), name='article-list'),
]

schema_view = get_swagger_view(title='CESED GapMaps API', url='/gapmaps')

urlpatterns = [
    path(r'site/', include(site_urls)),
    path(r'gapmaps/', include(gapmap_urls)),
    path(r'auth/', include('rest_framework.urls',
                               namespace='rest_framework')),
    path(r'get-token/', obtain_auth_token), 
    path(r'docs/', schema_view),
]

urlpatterns = format_suffix_patterns(urlpatterns)
