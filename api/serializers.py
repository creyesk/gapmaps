from rest_framework import serializers

from . models import Gapmap, Article, TopicModel

class TopicModelSerializer(serializers.ModelSerializer):
    """Serializer to map the TopicModel instance into JSON format."""

    class Meta:
        """Meta class to map serializer's fields with the model fields."""
        model = TopicModel
        fields = ('id', 'name', 'n_topics',
            'topic_names', 'tm_id')


class GapmapSerializer(serializers.ModelSerializer):
    """Serializer to map the Gapmap instance into JSON format."""
    subjects = TopicModelSerializer()
    methodologies = TopicModelSerializer()

    class Meta:
        """Meta class to map serializer's fields with the model fields."""
        model = Gapmap
        fields = ('id', 'name', 'heatmap', 'years', 'volume',
            'subjects_distribution', 'methodologies_distribution',
            'top_article_ids', 'top_article_ids_readable',
            'subjects', 'methodologies',)


class SimpleGapmapSerializer(serializers.ModelSerializer):
    """Serializer to map the Gapmap instance into JSON format."""

    class Meta:
        """Meta class to map serializer's fields with the model fields."""
        model = Gapmap
        fields = ('id', 'name',)

        
class ArticleSerializer(serializers.ModelSerializer):
    """Serializer to map the Survey instance into JSON format."""

    class Meta:
        """Meta class to map serializer's fields with the model fields."""
        model = Article 
        fields = ('id', 'title', 'date', 'keywords',)