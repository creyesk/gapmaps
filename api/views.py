from django.shortcuts import render
from django.http import HttpResponse
from rest_framework import generics, permissions, status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from .models import Gapmap


def base(request):
    return render(request, 'base.html')

def main(request):
    return render(request, 'main.html')

#@permission_classes((permissions.IsAuthenticated,))
@api_view(['GET'])
def topicsdata(request, pk):
    print(request, pk)
    try:
        gapmap = Gapmap.objects.get(id=pk)
        gapmap.load_lda_htmls()
        sub = gapmap.sub_lda
        met = gapmap.met_lda
    except Gapmap.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    # gapmap.load_objects()
    return Response(data={'subj': sub, 'method': met,},
                    status=status.HTTP_200_OK)