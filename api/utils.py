import numpy as np
from datetime import datetime

def normalize_rows(matrix, axis=1):
    norm = np.linalg.norm(matrix, axis=axis, ord=1)
    norm[norm == 0] = 1
    if axis==0:
        return matrix / norm[np.newaxis, :]
    elif axis==1:
        return matrix / norm[:, np.newaxis]

def pretty_id(date, author):
    return '{} [{}]'.format(author, date)

def get_year(date):
    d = datetime.strptime(date, '%Y-%m-%d')
    return d.year


