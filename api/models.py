from django.contrib.postgres.fields import ArrayField, JSONField
from django.core.paginator import Paginator
from django.db import models
from django.conf import settings

from sklearn.feature_extraction.text import CountVectorizer
from sklearn.decomposition import LatentDirichletAllocation as LDA
import pyLDAvis
from nltk.corpus import stopwords as stopw

from . utils import normalize_rows, pretty_id, get_year
from datetime import datetime
from collections import Counter, OrderedDict
import warnings
import os
import numpy as np

import pdb

class Section(models.Model):
    id = models.CharField(max_length=2048, primary_key=True)

    def __str__(self):
        return 'Section: ' + self.id


class SectionList(models.Model):
    id = models.CharField(max_length=32, primary_key=True)
    sections = models.ManyToManyField(Section, blank=True)
    def __str__(self):
        return self.id


class Article(models.Model):
    id = models.CharField(max_length=128, primary_key=True)
    title = models.CharField(max_length=11500)
    abstract = models.CharField(max_length=40000, default='')
    date = models.DateField(null=True)
    keywords = ArrayField(models.CharField(max_length=1024), default=list)
    author = models.CharField(max_length=2000, null=True, blank=True)
    full_text = JSONField(null=True, blank=True)
    section_text = JSONField(null=True, blank=True)

    def readable_id(self):
        first_author = self.author.split(' ')[0].replace(',', '')
        return first_author + '[' + str(self.date.year) + ']'

    def __str__(self):
        return 'Title: {}, i: {}'.format(self.title, self.id)


class Query(models.Model):
    query = models.CharField(max_length=2048)
    g_map = models.CharField(max_length=64, default='')
    articles = models.ManyToManyField(Article, blank=True)
    results =  ArrayField(models.CharField(max_length=256),
        null=True, blank=True)

    def __str__(self):
        return self.query


class Corpus(models.Model):
    name = models.CharField(max_length=128, default='')
    excluded_sections = models.ForeignKey(SectionList,
        default='Exclude', on_delete=models.CASCADE)
    queries = models.ManyToManyField(Query, blank=True) 
    text = ArrayField(models.CharField(max_length=1000000),
            null=True, blank=True)
    article_ids = ArrayField(models.CharField(max_length=64),
            null=True, blank=True)
    
    def get_text(self):
        print('Getting text from articles...')
        self.text = []
        self.article_ids = []
        exc_sections = [sec.id for sec in 
            self.excluded_sections.sections.all()]
        c = 0
        for q in self.queries.all():
            gen = (art for art in q.articles.all() 
                        if art.id not in self.article_ids) 
            for art in gen:
                c += 1
                t = art.abstract + ' ' + ' '.join(art.keywords)
                for sec in art.section_text:
                    if sec not in exc_sections:
                        t = t + '\n' + art.section_text[sec]
                self.text.append(t)
                self.article_ids.append(art.id)
        print('{} articles where added to corpus'.format(c))
    def __str__(self):
        return self.name


class Vocabulary(models.Model):
    name = models.CharField(max_length=64, primary_key=True)
    words = ArrayField(models.CharField(max_length=1024))
    stopwords = ArrayField(models.CharField(max_length=1024), default=list)
    query_words = ArrayField(models.CharField(max_length=1024), default=list)
    def __str__(self):
        return self.name 


class TopicModel(models.Model):
    name = models.CharField(max_length=32, unique=True, null=True)
    n_topics = models.IntegerField(default=10)
    tm_id = models.CharField(max_length=128, null=True)
    topic_names = ArrayField(models.CharField(max_length=512),
                                null=True)
    corpus = models.ForeignKey(Corpus, on_delete=models.CASCADE,
                               related_name='text_corpus')
    vocabulary = models.ForeignKey(Vocabulary, on_delete=models.CASCADE)
    vectorizer_params = JSONField(default={
        'min_df':0, 'max_df':1,
        'ngram_range':(1,2)})

    def generate_html(self, data):
        visid = 'vis_' + str(self.name)
        self.tm_id = visid
        f_name = '{}.html'.format(visid)
        path = os.path.join(settings.OBJECTS_FOLDER, f_name)
        data['sort_topics'] = False
        prepared_data = pyLDAvis.prepare(**data)
        vis = pyLDAvis.prepared_data_to_html(
            data=prepared_data, visid=visid)
        with open(path, 'w') as f:
            f.write(vis)
        self.save()

    def generate_topic_model(self, html=True):
        print('Generating {} topic model'.format(self.name))
        en_sw = stopw.words('english')
        vocab = set([x.lower() for x in self.vocabulary.words
                 if x.lower() not in self.vocabulary.stopwords])
        vectorizer = CountVectorizer(
                strip_accents='unicode',
                stop_words=self.vocabulary.stopwords + en_sw,
                decode_error='replace',
                vocabulary=list(vocab),
                **self.vectorizer_params)
        doc_term =  vectorizer.fit_transform(self.corpus.text)
        
        

        if self.vocabulary.query_words:
        # Give more weight to words that appear on queries

            query_weight = 250

            query_terms = np.isin(vectorizer.vocabulary,
                                  self.vocabulary.query_words)
            one_word = np.array([len(x.split(' '))<=1 
                                 for x in vectorizer.vocabulary])
            mask = (~one_word)*query_terms
            doc_term[:, mask] = doc_term[:, mask]*query_weight
        # remove excluded terms from doc_term and vocabulary  

        present_terms = (doc_term.sum(axis=0) > 0).getA1()
        present_vocab = np.array(vectorizer.vocabulary)[present_terms]
        doc_term = doc_term[:, present_terms]
        
        lda_model = LDA(n_components=self.n_topics,
                learning_method='batch', n_jobs=-1)
        doc_topic = lda_model.fit_transform(doc_term)

        topic_term_dists = normalize_rows(lda_model.components_, axis=1)
        # term_dist = comp.sum(axis=0)/comp.sum()
        # rel_term_topic_dist = term_topic_dist/term_dist[np.newaxis,:]
        term_proportion = doc_term.sum(axis=0) / doc_term.sum()
        lam = 0.8
        log_lift  = np.log(topic_term_dists / term_proportion)
        log_ttd   = np.log(topic_term_dists)
        relevance = lam*log_ttd + (1 - lam)*log_lift

        
        data = {'vocab': present_vocab.tolist(),
                'doc_lengths': doc_term.sum(axis=1).getA1().tolist(),
                'term_frequency': doc_term.sum(axis=0).getA1().tolist(),
                'doc_topic_dists': normalize_rows(doc_topic).tolist(),
                'topic_term_dists': topic_term_dists.tolist()}
        if html:
            self.generate_html(data)
        top_terms = relevance.argsort(axis=1)[:,-1].getA1()
        self.topic_names = present_vocab[top_terms].tolist()
        data['doc_topic'] = np.dot(doc_term.todense(), lda_model.components_.T)
        data['doc_term']  = doc_term
        self.save()
        return data


    def __str__(self):
        return self.name 


class Gapmap(models.Model):
    name = models.CharField(max_length=64, unique=True, default='')
    heatmap = ArrayField(ArrayField(models.FloatField()),
        null=True, blank=True)
    years = ArrayField(models.IntegerField(),
        null=True, blank=True)
    volume = ArrayField(models.IntegerField(),
        null=True, blank=True)
    top_article_ids = ArrayField(ArrayField(ArrayField(
        models.CharField(max_length=64))), null=True)
    top_article_ids_readable = ArrayField(ArrayField(ArrayField(
        models.CharField(max_length=512))), null=True)
    subjects = models.ForeignKey(TopicModel, null=True,
        related_name='subjects', on_delete=models.CASCADE)
    methodologies = models.ForeignKey(TopicModel, null=True,
        related_name='methodologies', on_delete=models.CASCADE)
    subjects_distribution = ArrayField(ArrayField(
        models.FloatField()), default=[[1.0, 2.0, 3.0],
                                       [1.0, 2.0, 3.0]])
    methodologies_distribution = ArrayField(ArrayField(
        models.FloatField()), default=[[1.0, 2.0, 3.0],
                                       [1.0, 2.0, 3.0]])

    def generate_gapmap(self):
        gap_threshold = 5
        sub_data = self.subjects.generate_topic_model(html=True)
        met_data = self.methodologies.generate_topic_model(html=True)
        
        
        # marginal distributions of subjects 
        # and methodologies in each document
        doc_sub = sub_data['doc_topic']
        doc_met = met_data['doc_topic']

        # topic_term_sub = np.array(sub_data['topic_term_dists'])
        # topic_term_met = np.array(met_data['topic_term_dists'])
        assert doc_sub.shape[0] == doc_met.shape[0], \
            'Corpus must be the same for both topic models'
        # norm_sub_met = np.dot(sub_doc_topic.T, met_doc_topic)

        # Distribution of documets in methodologies
        # and subjects 
        norm_met_sub = (np.dot(doc_met.T, doc_sub)/
                       (doc_sub.shape[0]**2))
        hmap = np.log(norm_met_sub+1)
        hmap[hmap<gap_threshold] = 0
        self.heatmap = hmap.tolist()
        
        # Joint distribution of subjects and
        # methodologies for each document
        doc_sub_met = np.einsum('ds, dm->dms', doc_sub, doc_met)
        top_articles = np.argsort(doc_sub_met, axis=0)[-1:-11:-1,...]
        top_articles = np.transpose(top_articles, axes=[1,2,0])
        
        article_ids = self.subjects.corpus.article_ids
        top_article_ids = np.array(article_ids)[top_articles]      
        
        pretty_ids = np.empty(top_article_ids.shape, dtype='object')
        for idx in np.ndindex(top_article_ids.shape):
            art = Article.objects.get(pk=top_article_ids[idx])
            pretty_ids[idx] = pretty_id(art.date, art.author)

        # p_id = [pretty_id(art.date, art.author) for art in articles]
        
        self.top_article_ids = top_article_ids.tolist()
        self.top_article_ids_readable = pretty_ids.tolist()

        # Years and Volume

        articles = (Article.objects.only('id','date','author')
                    .filter(pk__in=article_ids))
        years = [art.date.year for art in articles
                 if art.date.year >= 1990]
        od = OrderedDict(sorted(Counter(years).items()))
        self.years = list(od.keys())
        self.volume = list(od.values())

        self.save()

    def load_lda_htmls(self):
        s_name = '{}.html'.format(self.subjects.tm_id)
        s_path = os.path.join(settings.OBJECTS_FOLDER, s_name) 
        with open(s_path, 'r') as f:
            self.sub_lda = f.read()

        m_name = '{}.html'.format(self.methodologies.tm_id)
        m_path = os.path.join(settings.OBJECTS_FOLDER, m_name) 
        with open(m_path, 'r') as f:
            self.met_lda = f.read()    
    
    def __str__(self):
        return self.name



