# Elsevier downloader 2.0

from static.elsapy.elsapy.elsclient import ElsClient
from static.elsapy.elsapy.elsdoc import FullDoc
from static.elsapy.elsapy.elssearch import ElsSearch
from queries import build_queries
import gapmaps.settings as settings
import pdb
import argparse

import json, os

def download_articles(queries):
    for (gapmap, query) in queries:
        print('Searching for: {}.'.format(query))
        try:
            doc_srch = ElsSearch(query, 'scidir')
            doc_srch.execute(client, get_all=True)
            path = os.path.join('data', gapmap, 'queries')
            doc_srch.write(path)
            print('Search had {} results.'
                  .format(len(doc_srch.results)))

            for i, entry in enumerate(doc_srch.results):
                if len(doc_srch.results) > 1:
                    pii = entry['pii']
                    if pii not in pii_downloads:            
                        try:
                            pii_doc = FullDoc(sd_pii = pii,
                                              response_format='xml')
                            pii_doc.read(client)
                            path = os.path.join('data', gapmap, 'articles')
                            pii_doc.write(path)
                            pii_downloads.append(pii)
                            print('Download of document {}'
                                  ':{} was succesful.'
                                  .format(i,pii))
                        except Exception as e:
                            print('Download of document '
                                  '{} failed with {}'.format(pii, e))
                            pii_failed.append(pii)
                    else:
                        print('Document {} already '
                              'in database'.format(pii))
        except Exception as exc:
            print('Search failed with Error {}'.format(exc))
            pass
        

if __name__=='__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--subset", "-s",nargs='+', default='all')
    args = parser.parse_args()
    try:
        with open("downloaded_list.json") as f:
            dicty = json.load(f)
            pii_downloads = dicty['downloaded_pii']
            pii_failed = dicty['failed_pii']
    except IOError:
        pii_downloads = []
        pii_failed = []

    client = ElsClient(settings.API_KEY)

    queries = build_queries(subset=args.subset)
    download_articles(queries)

    with open("downloaded_list.json", 'w') as f:
        json.dump({"downloaded_pii":pii_downloads,
                   "failed_pii":pii_failed}, f)
