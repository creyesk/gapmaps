from itertools import product

"""
This module gives a dictionary used to build all the queries to be
performed by the elsapy.

Every gapamap has a corresponding key in queries_dict, the value to this 
key is another dictionary with the following items:

terms: phrases that will be search by Elsevier
cross_terms  cross_with: each pair of values i, j with i in cross_terms
     j in cross_with is searched for simultaneously in the same query.
    e.g. marijuana  consumers
exclude: terms that are not wanted in the search.
one_word_terms: This are important one word terms, that should be given the 
oportunity to appear as a topic in the gapmap. One word terms are excluded 
in the generation of the topic models since they usually have a greater 
weight than bigrams  are less informative. Only the words in this list 
will be considered as a monogram. 

"""

queries_dict = {
    'drugs':{
        'terms': [
            "narcotics trafficking",
            "narcotics crime",
            "narcotics distribution",
            "narcotics market",
            "narcotics production",
            "cocaine organized crime",
            "coca eradication",
            "illicit crops",
            "armed groups drugs",
            "armed groups",
            "manual eradication",
            "coca growers",
            "colombia cocaine",
            "mexico cocaine",
            'united states cocaine',
            "war on drugs",
            "plan colombia",
            "drug cartels",
            "cartel sinaloa",
            "el chapo",
            "los zetas",
            "downstream drug market",
            "drug abuse",
            "drug addiction",
            "drug policy",
            "illicit drug organized crime",
            "illicit drug trafficking",
            "illicit drug crime",
            "illicit drug economics",
            "illicit drug distribution",
            "illicit drug school",
            "recreational drug",
            "psychoactive substances",
        ],
        'cross_terms': [
                "marijuana",
                "metamphetamines",
                "amphetamines",
                "cocaine",
                "coca",
                "poppy",
                "opiods",
                "fentanyl",
                "crack cocaine",
                "cannabis",
                "alcohol",
                "poppers",
                "heroin",
                'lsd',
                '2cb',
        ],
        'cross_with': [
                "consumers",
                "prices",
                "trafficking",
                "crime",
                "economics",
                "distribution",
                "legalization",
                "market",
                "production",
                "school",
                "crops",
        ],
        'one_word_terms': [
           # "marijuana",
            "metamphetamines",
            #"amphetamines",
            "cocaine",
            #"opiods",
            #"fentanyl",
            #"cannabis",
            #"alcohol",
            #"poppers",
            "heroin",
            #'lsd',
            #'2cb'
        ],
        'exclude': [
            'prescription drug',
            'health services',
            'health care',
            'public health',
            'surface',
            'chromatography',
            'amino',
            'isoamyl',
            'carboxylic',
            'clinical',
            "ethyl",
            'acetic',
            'dioxide',
            'ethanol',
            "particle",
            "molar",
            "benzyl",
            "hormones",
            "cell",
            "dna",
            "protein ",
            "tumor",
            "cancer",
            "muscle",
            "immune system",
            "rat",
            "mice",
            "mouse",
            "fatty",
            "mehtyl",
            'tensile',
            "cellular",
            "radiotherapy",
            "endocrine",
            "ion",
            "coronary",
            'clinical trial',
            "antibiotic",
            'generic drug',
            "endocrine",
            "organophosphates",
            'medical devices',
            "diabetes",
            "artery",
            "fibrillation",
            "alzheimer",
            "cellulose",
            "leukemia ",
            "pharmacokinetics",
            "obesity",
            "hypothermia",
            'in vitro',
            "plasma",
            "ventricular",
            "laparoscopic",
            "lymphatic",
            "endovascular",
            "vascular",
            "gene",
            "molecular",
            'clinical drug',
        ]
    },
    'crime': {
        'terms': [
            "international criminal justice",
            "crime",
            "criminal",
            "crime rates",
            "crime economics",
            "crime prediction",
            "violent crimes",
            "delinquency",
            "juvenile delinquency",
            "cyber crime",
            "law enforcement",
            "homicide",
            "homicide rates",
            "youth homicides",
            "homicide firearm",
            "homicide economics",
            "female homicide",
            "gang homicide",
            "homicide forensic",
            "death violence",
            "victims homicide",
            "death gang",
            "death gun",
            "death firearm",
            "police",
            "police effect",
            "police policing",
            "police impact",
            "police intervention",
            "police program",
            "police conflict",
            "police neighborhoods",
            "army conflict",
            "military conflict",
            "prison crime",
            "prison recidivism",
            "prison discrimination",
            "prison experiment",
            "prison impact",
            "incarceration crime",
            "incarceration recidivism",
            "incarceration discrimination",
            "imprisonment crime",
            "imprisonment recidivism",
            "imprisonment discrimination",

            "amnesty",
            "assault",
            "aggravated assault",
            "alcohol crime",
            "acquittal",
            "convicts",
            "urban crime",
            "crime hotspots",
            "crime prevention",
            "crime control",
            "criminal trials",
            "criminal activity",
            "criminal procedure",
            "criminal offenses",
            "organized crime",
            "criminal organizations",
            "war crimes",
            "sexual crime",
            "criminal networks",
            "capital crime",
            "juvenile crime",
            "crime policies",
            "education crime",
            "immigration crime",
            "gender crime",
            "crime deterrence",
            "crime labor markets",
            "crime risk",
            "crime incidence",
            "crime levels",
            "criminalisation",
            "internatonal criminal law",
            "death penalty",
            "felony",
            "incrimination",
            "Impunity",
            "law-abiding",
            "law enforcement",
            "misdemeanor",
            "manslaughter",
            "murder",
            "murderer",
            "offenders",
            "ex offenders",
            "capital offense",
            "police effectivness",
            "police officer",
            "police services",
            "policing technologies",
            "police custody",
            "penal",
            "plea bargaining",
            "probation",
            "trafficking",
            "firearms trafficking",
            "arms trafficking",
            "gangs violennce",
            "smuggling",
            "theft",
            "repeat offenders",
            "vandalism",
        ],
        'cross_terms':[],
        'cross_with': [],
        'one_word_terms':[],
        'exclude': [],
    }
}

def build_queries(d=queries_dict, subset='all'):
    """
    This function takes the queries_dict dictionary from this module 
    returns a list from all the queries to be executed by elsapy.

    A list of gapamaps may be specified as a list to subset in order to only
    consider the queries for that gapmap.
    """
    if subset=='all':
        subset = d.keys()
    queries = []
    for k, v in d.items():
        if k in subset:
            exclusions = ' AND NOT '.join(v['exclude'])
            for term in v['terms']:
                term = ' AND '.join(term.split(' '))
                if exclusions != '':
                    query = "tak({} AND NOT {})"
                    query = query.format(term, exclusions)
                else:
                    query = "tak({})".format(term)
                queries.append((k, query))
            for term, w in product(v['cross_terms'], v['cross_with']):
                if exclusions != '':
                    query = "tak({} AND {} AND NOT {})"
                    query = query.format(term, w, exclusions)
                else:
                    query = "tak({} AND {})".format(term, w)
                queries.append((k, query))
    return queries