from api.models import Article, Section, Query
from api.models import Corpus, Vocabulary, TopicModel

import gapmaps.settings as settings

from static.elsapy.elsapy.elsclient import ElsClient
from static.elsapy.elsapy.elssearch import ElsSearch
from static.elsapy.elsapy.elsdoc import FullDoc

from utils import _extract, _parse_keywords, _parse_sections
from utils import add_article, query_results
from api.utils import get_year
from load import data_dir
import os, re, json
from tqdm import tqdm

from collections import defaultdict, Counter

def update_queries(overwrite=False, data_dir=data_dir):
    """
    Executes every Query and saves the results in the Query model.
    """
    print('Updating queries...')
    client = ElsClient(settings.API_KEY)
    for c in Corpus.objects.all():
        k = re.findall(r'^([a-z]+)_', c.name)[0]
        for q in c.queries.all():
            print('Updating query:', q.query)
            query_dir = os.path.join(data_dir, k, 'queries')
            saved_results = query_results(q.query, query_dir)
            if saved_results is not None and not overwrite: 
                q.results = saved_results
            else:
                doc_srch = ElsSearch(q.query, 'scidir')
                doc_srch.execute(client, get_all = True)
                try:
                    q.results = [entry['pii'] for entry in doc_srch.results]
                    print("Search had {} results".format(len(doc_srch.results)))
                except:
                    print('Search had no results')
                finally:
                    doc_srch.write(query_dir)
                
            q.save()

def update_articles():
    """
    Loops over the articles in store in the results field of every
    query. Checks if the article is already in the database and
    downloads it in case it isn't. (Will only download abstract if 
    there is no permission.)

    Links all articles that belong to each query. 
    """
    print('Updating articles')
    client = ElsClient(settings.API_KEY)
    failed = []
    for q in Query.objects.all():
        print('Updating articles in query', q.query)
        for art in tqdm(q.results):
            if not Article.objects.filter(pk=art).exists():
                pii_doc = FullDoc(sd_pii=art, response_format='xml')
                print(f'Retrieving article: {art}')
                pii_doc.read(client)
                # try:                    
                add_article(pii_doc.data)
                pii_doc.write(os.path.join(data_dir, q.g_map, 'articles'))
                # except:
                #     print(f'Failed to add article {art}')
                #     failed.append(art)

                print('Article {} added succesfully'.format(art))
        art_q = Article.objects.filter(pk__in=q.results)
        q.articles.set(art_q, clear=True)
        q.save()
    # with open('failed.json', 'w') as f:
    #     json.dump(failed, f)

def update_sections(min_df=100):
    """
    Searches the text of all articles and extracts the section titles
    of each one. If there are at least min_df number of articles with
    that section title then the section is a Section.
    """
    print('Updating sections...')
    Section.objects.all().delete()
    articles = Article.objects.all().iterator()
    sec_dict =  defaultdict(int)
    for i, art in enumerate(articles):
        print('Processing article', i, end="\r")
        for k in art.section_text.keys():
            sec_dict[k.lower()] += 1
    to_add = [k for k, v in sec_dict.items() if v>min_df]
    print('Adding {} sections'.format(len(to_add)))
    for k in to_add:
        s = Section(id=k)
        s.save()

def update_corpora(subset=False):
    """
    For each Corpus object (containg queries and its articles). Search all the 
    text in those articles excluding the sections in the ExcludedSections object.
    Saves the text in the corpus object as an array of strings.
    """
    if subset:
        to_update = Corpus.objects.filter(pk__in=subset).iterator()
    else:
        to_update = Corpus.objects.all().iterator() 
    for c in to_update:
        print('Updating Corpus {}...'.format(c.name))
        c.get_text()
        c.save()
